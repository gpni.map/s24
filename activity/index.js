console.log("Activity");

// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:
	console.log(" ")

	let cube = 2 ** 3

	console.log(`The cube of 2 is ${cube}`);



	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:
	console.log(" ")

	let address = ["The Grand Tower-Manila", "790 P, Ocampo Street", "Manila", 1004];

	const [building, street, city, zipCode] = address

	console.log (`I live at ${building}, ${street}, ${city}, ${zipCode}`);


	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:
	console.log(" ")

	let animal = {
		animalName: "Scan",
		animalType: "dog",
		animalOwner: "Mira"
	}

	const {animalName, animalType, animalOwner} = animal;

	console.log(`My niece, ${animalOwner}, has a ${animalType} named ${animalName}`);


	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:

	console.log(" ")

	const randomNumbers = [1, 5, 7, 23, 45, 2, 18];

	randomNumbers.forEach((number) => {
		console.log(`${number}`)
		
		});




/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:
	
	console.log(" ")

	class Dog {
		constructor(name, age, breed){
			this.name = name
			this.age = age
			this.breed = breed
		}
	}

	const myDog = new Dog('Lou', 2, 'Rotweiller')
	console.log(myDog);